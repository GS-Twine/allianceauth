# Generated by Django 3.1.13 on 2021-10-15 05:02

from django.db import migrations


def modify_aa_team_token_add_page_ignore_paths(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.

    AnalyticsPath = apps.get_model('analytics', 'AnalyticsPath')
    admin = AnalyticsPath.objects.create(ignore_path=r"^\/admin\/.*")
    user_notifications_count = AnalyticsPath.objects.create(ignore_path=r"^\/user_notifications_count\/.*")

    Tokens = apps.get_model('analytics', 'AnalyticsTokens')
    token = Tokens.objects.get(token="UA-186249766-2")
    token.ignore_paths.add(admin, user_notifications_count)


def undo_modify_aa_team_token_add_page_ignore_paths(apps, schema_editor):
    # nothing should need to migrate away here?
    return True


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0003_Generate_Identifier'),
    ]

    operations = [migrations.RunPython(modify_aa_team_token_add_page_ignore_paths, undo_modify_aa_team_token_add_page_ignore_paths)
    ]
